# UML-DOP diagram to WinVMJ (U2Win) Generator 

## Intro
The UML-DOP diagram is a UML diagram that uses the UML-DOP profile, which supports delta-oriented software product line. The UML-DOP profile enables delta-oriented notation in the UML diagram.

VMJ is an architectural pattern in Java, designed using design patterns and Java module system.
VMJ also supports delta-oriented progamming. The VMJ structure consists of the core modules, the delta modules, and the product modules.  WinVMJ is a web framework based on VMJ. The structure of the WinVMJ consists of domain layer, presentation layer, and persistence layer. 

This project consists of generator from the UML-DOP diagram to WinVMJ (U2Win). The generator is built
using Eclipse Acceleo Model to Text Transformator.
The generated source code is a part of domain layer in the WinVMJ framework.

## Requirements
1. Eclipse Modeling Tools 2020-12
2. UML Editor (Papyrus version 2020-12) [Update Site](https://download.eclipse.org/modeling/mdt/papyrus/updates/releases/2020-12)
3. Acceleo 3.7 - [Update Site](http://download.eclipse.org/acceleo/updates/releases/3.7)


<!---
## How to Use
1. Download the U2VMJ Plugin from this [Release Page](https://gitlab.com/RSE-Lab-Fasilkom-UI/PricesIDE/uml-to-vmj/-/releases). Read how to install from the Zip files.
2. The UML-DOP diagram is modeled with the UML-DOP profile using Papyrus.
   - Read [here](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/model-transformation/uml-dop-profile) to know how to use the UML-DOP profile.
   - You can also download the examples of the UML-DOP diagram from the
[AISCO](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/model-transformation/aisco-uml-dop) case study.
3. Right click on the .uml file and choose Generate VMJ. -->

## For the Developer
1. Clone this repository and import folder uml.to.vmj as Eclipse project.
2. Run the tools via Run Configuration menu. Specify the .uml files and the output folder.
3. Create a new branch to modify the generator.
